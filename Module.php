<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions;

    use DoctrineModule\Persistence\ObjectManagerAwareInterface;
    use Zend\ModuleManager\Feature\ConfigProviderInterface;
    use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
    use Zend\Mvc\MvcEvent;
    use Zend\ServiceManager\ServiceLocatorAwareInterface;
    use Zend\Stdlib\InitializableInterface;

    /**
     * Zend framework 2 module configuration for PhproDoctrineExtensions
     *
     * @category               Module
     * @package                PhproDoctrineExtensions
     * @author                 PHPro <info@phpro.be>
     * @license                http://opensource.org/licenses/gpl-license.php GNU Public License
     * @version                1.0
     *
     */
    class Module implements AutoloaderProviderInterface, ConfigProviderInterface
    {
        /**
         * The config key in the service manager
         */
        const CONFIG_KEY = 'PhproDoctrineExtensionsConfig';

        /**
         * Adds custom DQL function options
         *
         * @param \Zend\Mvc\MvcEvent $e
         */
        public function onBootstrap(MvcEvent $e)
        {
            $application    = $e->getApplication();
            $serviceManager = $application->getServiceManager();

            /** @var \Doctrine\ORM\EntityManager $em */
            $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

            if (defined('APPLICATION_ENV')) {
                $cacheDriver = $entityManager->getMetadataFactory()->getCacheDriver();
                $cacheDriver->setNamespace(APPLICATION_ENV);
            }

            $serviceManager->get(__NAMESPACE__ . '\Feature\Blameable');
            $serviceManager->get(__NAMESPACE__ . '\Feature\Timestampable');

            $config = $entityManager->getConfiguration();
            $config->addCustomStringFunction('MD5', 'PhproDoctrineExtensions\Query\MySql\Md5');
            $config->addCustomStringFunction('GROUP_CONCAT', 'PhproDoctrineExtensions\Query\MySql\GroupConcat');
            $config->addCustomStringFunction('REPLACE', 'PhproDoctrineExtensions\Query\MySql\Replace');
        }

        /**
         * Initialises classes which implement the EntityManagerAwareInterface and the ObjectManagerAwareInterface
         * with the doctrine entityManager
         *
         * @return array
         */
        public function getServiceConfig()
        {
            return array(
                'initializers' => array(
                    function ($instance, $serviceManager) {
                        if ($instance instanceof ServiceLocatorAwareInterface && !$instance->getServiceLocator()) {
                            $instance->setServiceLocator($serviceManager);
                        }
                        if ($instance instanceof ObjectManagerAwareInterface) {
                            $instance->setObjectManager($serviceManager->get('Doctrine\ORM\EntityManager'));
                        }
                        if ($instance instanceof InitializableInterface) {
                            $instance->init();
                        }
                    }
                ),
            );
        }

        /**
         * @inheritdoc
         */
        public function getConfig($env = null)
        {
            return include __DIR__ . '/config/module.config.php';
        }

        /**
         * @inheritdoc
         */
        public function getAutoloaderConfig()
        {
            return array(
                'Zend\Loader\StandardAutoloader' => array(
                    'namespaces' => array(
                        __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    ),
                ),
            );
        }

    }
