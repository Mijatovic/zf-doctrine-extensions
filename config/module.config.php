<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions;
    return array(
        'service_manager' => array(
            'invokables'          => [
                __NAMESPACE__ . '\Feature\Timestampable' => __NAMESPACE__ . '\Feature\Timestampable',
                __NAMESPACE__ . '\Feature\Blameable' => __NAMESPACE__ . '\Feature\Blameable',
            ],
        ),
        /**
         * PhproDoctrineExtensions configuration
         */
        'PhproDoctrineExtensionsConfig' => array(

            'timestampable' => array(
                /**
                 * Enable blameable listener
                 */
                'enabled'               => false,
            ),

            'blameable' => array(
                /**
                 * Enable blameable listener
                 */
                'enabled'               => false,
                /**
                 *  The authentication service that should be used
                 *  Should be istance of \Zend\Authentication\AuthenticationService
                 */
                'authenticationService' => 'zfcuser_auth_service',
            )
        ),
    );
