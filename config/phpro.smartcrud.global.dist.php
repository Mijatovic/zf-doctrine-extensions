<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions;

    return array(
        /**
         * PhproDoctrineExtensions configuration
         */
        'PhproDoctrineExtensionsConfig' => array(
            'timestampable' => array(
                /**
                 * Enable timeStampable listener
                 */
                'enabled'               => true,
            ),
            'blameable' => array(
                /**
                 * Enable blameable listener
                 */
                'enabled'               => true,
                /**
                 *  The authentication service that should be used
                 *  Should be istance of \Zend\Authentication\AuthenticationService
                 */
                'authenticationService' => 'zfcuser_auth_service',
            )
        ),

    );
