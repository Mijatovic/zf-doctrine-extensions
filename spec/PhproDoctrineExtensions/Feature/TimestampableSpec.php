<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace spec\PhproDoctrineExtensions\Feature;

    use PhpSpec\ObjectBehavior;
    use Prophecy\Argument;

    class TimestampableSpec extends ObjectBehavior
    {

        public function it_should_be_a_feature()
        {
            $this->shouldHaveType('PhproDoctrineExtensions\Feature\Timestampable');
            $this->shouldBeAnInstanceOf('PhproDoctrineExtensions\Feature\AbstractFeature');
            $this->shouldReturnAnInstanceOf('PhproDoctrineExtensions\Feature\Timestampable');
            $this->shouldImplement('DoctrineModule\Persistence\ObjectManagerAwareInterface');
            $this->shouldImplement('Zend\ServiceManager\ServiceLocatorAwareInterface');
            $this->shouldImplement('Zend\Stdlib\InitializableInterface');
        }
        /**
         * @param \Zend\ServiceManager\ServiceManager $serviceManager
         */
        function it_should_not_initialize_the_disabled_feature($serviceManager)
        {
            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'timestampable' => array(
                        'enabled'               => false,
                    )
                )
            );

            $serviceManager->get('Config')->willReturn($config);
            $this->setServiceLocator($serviceManager);

            $this->init()->shouldReturn(false);
        }

        /**
         * @param \Zend\ServiceManager\ServiceManager $serviceManager
         * @param \Doctrine\ORM\EntityManager $objectManager
         * @param \Doctrine\Common\EventManager $eventManager
         */
        function it_should_initialize_the_feature($serviceManager, $objectManager, $eventManager)
        {
            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'timestampable' => array(
                        'enabled'               => true,
                    )
                )
            );

            $serviceManager->get('Config')->willReturn($config);
            $serviceManager->get('Config')->shouldBeCalled();
            $this->setServiceLocator($serviceManager);

            $objectManager->getEventManager()->willReturn($eventManager);
            $objectManager->getEventManager()->shouldBeCalled();
            $this->setObjectManager($objectManager);

            $eventManager->addEventSubscriber(Argument::type('\Gedmo\Timestampable\TimestampableListener'))->shouldBeCalled();

            $this->init()->shouldReturn(true);
        }
    }
