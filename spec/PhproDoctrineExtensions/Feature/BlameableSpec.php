<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace spec\PhproDoctrineExtensions\Feature;

    use PhpSpec\ObjectBehavior;
    use Prophecy\Argument;

    class BlameableSpec extends ObjectBehavior
    {

        public function it_should_be_a_feature()
        {
            $this->shouldHaveType('PhproDoctrineExtensions\Feature\Blameable');
            $this->shouldBeAnInstanceOf('PhproDoctrineExtensions\Feature\AbstractFeature');
            $this->shouldReturnAnInstanceOf('PhproDoctrineExtensions\Feature\Blameable');
            $this->shouldImplement('DoctrineModule\Persistence\ObjectManagerAwareInterface');
            $this->shouldImplement('Zend\ServiceManager\ServiceLocatorAwareInterface');
            $this->shouldImplement('Zend\Stdlib\InitializableInterface');
        }

        /**
         * @param \Zend\ServiceManager\ServiceManager $serviceManager
         */
        function it_should_not_initialise_a_disabled_feature($serviceManager)
        {
            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'blameable' => array(
                        'enabled' => false,
                    )
                )
            );

            $serviceManager->get('Config')->willReturn($config);
            $serviceManager->get('Config')->shouldBeCalled();

            $this->setServiceLocator($serviceManager);

            $this->init()->shouldReturn(false);
        }

        /**
         * @param \Zend\ServiceManager\ServiceManager $serviceManager
         * @param \Doctrine\ORM\EntityManager         $objectManager
         * @param \Doctrine\Common\EventManager       $eventManager
         */
        function it_should_throw_an_exception_when_configuration_is_not_ok(
            $serviceManager,
            $objectManager, $eventManager
        ) {
            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'blameable' => array(
                        'enabled' => true,
                    )
                )
            );

            $objectManager->getEventManager()->willReturn($eventManager);
            $objectManager->getEventManager()->shouldNotBeCalled();
            $this->setObjectManager($objectManager);

            $serviceManager->get('Config')->willReturn($config);
            $serviceManager->get('Config')->shouldBeCalled();
            $this->setServiceLocator($serviceManager);
            $this->shouldThrow('\PhproDoctrineExtensions\Exception\ConfigurationException')->duringInit();
        }

        /**
         * @param \Zend\ServiceManager\ServiceManager        $serviceManager
         * @param \Doctrine\ORM\EntityManager                $objectManager
         * @param \Doctrine\Common\EventManager              $eventManager
         * @param \Zend\Authentication\AuthenticationService $authService
         */
        function it_should_initialize_with_the_user_from_the_auth_service(
            $serviceManager,
            $objectManager, $eventManager, $authService
        ) {

            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'blameable' => array(
                        'enabled'               => true,
                        'authenticationService' => 'an_auth_service_key',
                    )
                )
            );

            $authService->hasIdentity()->willReturn(true);
            $authService->hasIdentity()->shouldBeCalled();

            $authService->getIdentity()->willReturn(new \StdClass());
            $authService->getIdentity()->shouldBeCalled();

            $serviceManager->get('Config')->willReturn($config);
            $serviceManager->has('an_auth_service_key')->willReturn(true);
            $serviceManager->has('an_auth_service_key')->shouldBeCalled();
            $serviceManager->get('an_auth_service_key')->willReturn($authService);
            $serviceManager->get('an_auth_service_key')->shouldBeCalled();
            $this->setServiceLocator($serviceManager);

            $eventManager->addEventSubscriber(Argument::type('\Gedmo\Blameable\BlameableListener'))
                ->shouldBeCalled();

            $objectManager->getEventManager()->willReturn($eventManager);
            $objectManager->getEventManager()->shouldBeCalled();
            $this->setObjectManager($objectManager);

            $this->init()->shouldReturn(true);
        }

        /**
         * @param \Zend\ServiceManager\ServiceManager        $serviceManager
         * @param \Doctrine\ORM\EntityManager                $objectManager
         * @param \Doctrine\Common\EventManager              $eventManager
         * @param \Zend\Authentication\AuthenticationService $authService
         */
        function it_should_not_initialize_when_auth_service_has_no_user(
            $serviceManager,
            $objectManager, $eventManager, $authService
        ) {

            $config = array(
                'PhproDoctrineExtensionsConfig' => array(
                    'blameable' => array(
                        'enabled'               => true,
                        'authenticationService' => 'an_auth_service_key',
                    )
                )
            );

            $authService->hasIdentity()->willReturn(false);
            $authService->hasIdentity()->shouldBeCalled();

            $authService->getIdentity()->willReturn(new \StdClass());
            $authService->getIdentity()->shouldNotBeCalled();

            $serviceManager->get('Config')->willReturn($config);
            $serviceManager->has('an_auth_service_key')->willReturn(true);
            $serviceManager->has('an_auth_service_key')->shouldBeCalled();
            $serviceManager->get('an_auth_service_key')->willReturn($authService);
            $serviceManager->get('an_auth_service_key')->shouldBeCalled();
            $this->setServiceLocator($serviceManager);

            $eventManager->addEventSubscriber(Argument::type('\Gedmo\Blameable\BlameableListener'))
                ->shouldNotBeCalled();

            $objectManager->getEventManager()->shouldNotBeCalled();
            $this->setObjectManager($objectManager);


            $this->init()->shouldReturn(true);
        }
    }
