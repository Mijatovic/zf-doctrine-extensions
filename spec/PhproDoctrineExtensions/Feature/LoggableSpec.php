<?php

namespace spec\PhproDoctrineExtensions\Feature;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LoggableSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('PhproDoctrineExtensions\Feature\Loggable');
    }
    public function it_should_be_a_feature()
    {
        $this->shouldHaveType('PhproDoctrineExtensions\Feature\Loggable');
        $this->shouldBeAnInstanceOf('PhproDoctrineExtensions\Feature\AbstractFeature');
        $this->shouldReturnAnInstanceOf('PhproDoctrineExtensions\Feature\Loggable');
        $this->shouldImplement('DoctrineModule\Persistence\ObjectManagerAwareInterface');
        $this->shouldImplement('Zend\ServiceManager\ServiceLocatorAwareInterface');
        $this->shouldImplement('Zend\Stdlib\InitializableInterface');
    }
    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function it_should_not_initialize_the_disabled_feature($serviceManager)
    {
        $config = array(
            'PhproDoctrineExtensionsConfig' => array(
                'loggable' => array(
                    'enabled'               => false,
                )
            )
        );

        $serviceManager->get('Config')->willReturn($config);
        $this->setServiceLocator($serviceManager);

        $this->init()->shouldReturn(false);
    }

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @param \Doctrine\ORM\EntityManager $objectManager
     * @param \Doctrine\Common\EventManager $eventManager
     */
    public function it_should_initialize_the_feature_when_enabled($serviceManager, $objectManager, $eventManager)
    {
        $config = array(
            'PhproDoctrineExtensionsConfig' => array(
                'loggable' => array(
                    'enabled'               => true,
                )
            )
        );

        $objectManager->getEventManager()->willReturn($eventManager);
        $objectManager->getEventManager()->shouldBeCalled();
        $this->setObjectManager($objectManager);

        $serviceManager->get('Config')->willReturn($config);
        $serviceManager->get('Config')->shouldBeCalled();
        $this->setServiceLocator($serviceManager);

        $eventManager->addEventSubscriber(Argument::type('\Gedmo\Loggable\LoggableListener'))->shouldBeCalled();

        $this->init()->shouldReturn(true);
    }

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @param \Doctrine\ORM\EntityManager $objectManager
     * @param \Doctrine\Common\EventManager $eventManager
     * @param \Zend\Authentication\AuthenticationService $authService
     */
    public function it_should_initialize_listener_with_current_user($serviceManager, $objectManager,
        $eventManager, $authService)
    {
        $config = array(
            'PhproDoctrineExtensionsConfig' => array(
                'loggable' => array(
                    'enabled'               => true,
                    'authenticationService' => 'an_auth_service_key',
                )
            )
        );

        $authService->hasIdentity()->willReturn(true);
        $authService->hasIdentity()->shouldBeCalled();

        $authService->getIdentity()->willReturn(new \StdClass());
        $authService->getIdentity()->shouldBeCalled();

        $objectManager->getEventManager()->willReturn($eventManager);
        $objectManager->getEventManager()->shouldBeCalled();
        $this->setObjectManager($objectManager);

        $serviceManager->get('Config')->willReturn($config);
        $serviceManager->get('Config')->shouldBeCalled();
        $serviceManager->has('an_auth_service_key')->willReturn(true);
        $serviceManager->has('an_auth_service_key')->shouldBeCalled();
        $serviceManager->get('an_auth_service_key')->willReturn($authService);
        $serviceManager->get('an_auth_service_key')->shouldBeCalled();
        $this->setServiceLocator($serviceManager);

        $eventManager->addEventSubscriber(Argument::type('\Gedmo\Loggable\LoggableListener'))->shouldBeCalled();

        $this->init()->shouldReturn(true);
    }
}
