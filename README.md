# Doctrine Extensions for Zend Framework 2

### AbstractEntity class:

Blue print class to build entities from.

## Features:

### Timestampable:

ZF2 implementation of https://github.com/l3pp4rd/DoctrineExtensions/blob/master/doc/timestampable.md

Timestampable behavior will automate the update of date fields on your Entities or Documents. It works through annotations and can update fields on creation, update, property subset update, or even on specific property value change.

- Entities which should be timestampable should implement the TimestampableInterface
- Timestampable feature should be enabled in the configuration

``` php
<?php
    return array(
        'PhproDoctrineExtensionsConfig' => array(
            'timestampable' => array(
                'enabled'               => true,
            ),
        ),
    );
```

### Blameable:

ZF2 implementation of https://github.com/l3pp4rd/DoctrineExtensions/blob/master/doc/blameable.md

Blameable behavior will automate the update of username or user reference fields on your Entities or Documents. It works through annotations and can update fields on creation, update, property subset update, or even on specific property value change.

- Entities which should be timestampable should implement the BlameableInterface
- Blameable feature should be enabled in the configuration

``` php
<?php
    return array(
        'PhproDoctrineExtensionsConfig' => array(
            'blameable' => array(
                'enabled'               => true,
                'authenticationService' => 'zfcuser_auth_service',
            )
        ),
    );
```
### loggable:

ZF2 implementation of [Loggable behavior](https://github.com/l3pp4rd/DoctrineExtensions/blob/master/doc/loggable.md)

Loggable behavior tracks your record changes and is able to manage versions.

- Loggable feature should be enabled in the configuration

``` php
<?php
    return array(
        'PhproDoctrineExtensionsConfig' => array(
            'loggable' => array(
                'enabled'               => true,
                'authenticationService' => 'zfcuser_auth_service',
            )
        ),
    );
```