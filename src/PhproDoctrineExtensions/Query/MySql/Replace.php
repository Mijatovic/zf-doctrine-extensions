<?php

    /**
     * DoctrineExtensions Mysql Function Pack
     *
     * LICENSE
     *
     * This source file is subject to the new BSD license that is bundled
     * with this package in the file LICENSE.txt.
     * If you did not receive a copy of the license and are unable to
     * obtain it through the world-wide-web, please send an email
     * to kontakt@beberlei.de so I can send you a copy immediately.
     */

    namespace PhproDoctrineExtensions\Query\MySql;

    use Doctrine\ORM\Query\AST\Functions\FunctionNode;
    use Doctrine\ORM\Query\Lexer;
    use Doctrine\ORM\Query\Parser;
    use Doctrine\ORM\Query\SqlWalker;

    /**
     * REPLACE(str, from_str, to_str)
     * "REPLACE" "(" StringPrimary "," StringPrimary "," StringPrimary ")"
     */
    class Replace extends FunctionNode
    {
        protected $stringStr, $stringFromStr, $stringToStr;

        public function getSql(SqlWalker $sqlWalker)
        {
            return sprintf(
                'REPLACE(%s, %s, %s)',
                $sqlWalker->walkStringPrimary($this->stringStr),
                $sqlWalker->walkStringPrimary($this->stringFromStr),
                $sqlWalker->walkStringPrimary($this->stringToStr)
            );
        }

        public function parse(Parser $parser)
        {
            $parser->match(Lexer::T_IDENTIFIER);                // REPLACE
            $parser->match(Lexer::T_OPEN_PARENTHESIS);
            $this->stringStr = $parser->StringPrimary();        // str
            $parser->match(Lexer::T_COMMA);
            $this->stringFromStr = $parser->StringPrimary();    // from_str
            $parser->match(Lexer::T_COMMA);
            $this->stringToStr = $parser->StringPrimary();      // to_str
            $parser->match(Lexer::T_CLOSE_PARENTHESIS);
        }
    }