<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Entity;

    // Doctrine Namespace Imports
    use Doctrine\ORM\Mapping;
    use Gedmo\Mapping\Annotation as Gedmo;

    /**
     * Trait for Timestample fields.
     *
     * @category               Entity
     * @package                PhproDoctrineExtensions
     * @author                 PHPro <info@phpro.be>
     * @license                http://opensource.org/licenses/gpl-license.php GNU Public License
     * @version                1.0
     *
     */
    trait ProvidesTimeStampableTrait
    {

        /**
         * @var string
         *
         * @Gedmo\Timestampable(on="create")
         *
         * @Mapping\Column(
         *  name="created_at",
         *  type="datetime",
         *  nullable=true);
         */
        protected $createdAt;
        /**
         * @var string
         *
         * @Gedmo\Timestampable(on="update")
         *
         * @Mapping\Column(
         *  name="updated_at",
         *  type="datetime",
         *  nullable=true);
         */
        protected $updatedAt;

        /**
         * Returns createdAt.
         *
         * @return DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Sets createdAt.
         *
         * @param  DateTime $createdAt
         *
         * @return $this
         */
        public function setCreatedAt(\DateTime $createdAt)
        {
            $this->createdAt = $createdAt;

            return $this;
        }

        /**
         * Returns updatedAt.
         *
         * @return DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * Sets updatedAt.
         *
         * @param  DateTime $updatedAt
         *
         * @return $this
         */
        public function setUpdatedAt(\DateTime $updatedAt)
        {
            $this->updatedAt = $updatedAt;

            return $this;
        }
    }
