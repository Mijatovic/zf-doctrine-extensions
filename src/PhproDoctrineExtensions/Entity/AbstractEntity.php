<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Entity;

    // Doctrine Namespace Imports
    use Doctrine\ORM\Mapping;

    /**
     * Abstract entity class to extend doctrine entities from.
     *
     * @category               Entity
     * @package                PhproDoctrineExtensions
     * @author                 PHPro <info@phpro.be>
     * @license                http://opensource.org/licenses/gpl-license.php GNU Public License
     * @version                1.0
     *
     */
    abstract class AbstractEntity
    {
        /**
         * Entity ID
         *
         * @var int
         *
         * @Mapping\Id
         * @Mapping\Column(name="id", type="integer");
         * @Mapping\GeneratedValue(strategy="IDENTITY")
         */
        protected $id;

        /**
         * Get the id of the entity
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set the id of the entity
         *
         * @param int $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         *
         */
        public function __clone()
        {
            $this->id = null;
        }

        /**
         * delegates initialization of the collections
          */
        public function __construct()
        {
            $this->initializeCollections();
        }

        /**
         * Collections that need to be initialized on construction of the object
         *
         * @return mixed
         */
        abstract public function initializeCollections();

    }
