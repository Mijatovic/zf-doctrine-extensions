<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Entity;

    // Doctrine Namespace Imports
    use Doctrine\ORM\Mapping;
    use Gedmo\Mapping\Annotation as Gedmo;

    /**
     * Trait for Timestample fields.
     *
     * @category               Entity
     * @package                PhproDoctrineExtensions
     * @author                 PHPro <info@phpro.be>
     * @license                http://opensource.org/licenses/gpl-license.php GNU Public License
     * @version                1.0
     *
     */
    trait ProvidesBlameableTrait
    {

        /**
         * @var string
         *
         * @Gedmo\Blameable(on="create")
         *
         * @Mapping\Column(
         *  name="created_by",
         *  type="string",
         *  nullable=true);
         */
        protected $createdBy;
        /**
         * @var string
         *
         * @Gedmo\Blameable(on="update")
         *
         * @Mapping\Column(
         *  name="updated_by",
         *  type="string",
         *  nullable=true);
         */
        protected $updatedBy;



    }
