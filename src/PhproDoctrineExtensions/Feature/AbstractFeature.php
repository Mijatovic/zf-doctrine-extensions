<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Feature;

    use DoctrineModule\Persistence\ObjectManagerAwareInterface;
    use DoctrineModule\Persistence\ProvidesObjectManager;
    use Zend\ServiceManager\ServiceLocatorAwareTrait;
    use Zend\ServiceManager\ServiceLocatorAwareInterface;
    use Zend\Stdlib\InitializableInterface;

    abstract class AbstractFeature
        implements ServiceLocatorAwareInterface, ObjectManagerAwareInterface, InitializableInterface
    {
        use ServiceLocatorAwareTrait, ProvidesObjectManager;

        /**
         * The config key in the service manager
         */
        const CONFIG_KEY = 'PhproDoctrineExtensionsConfig';
        /**
         * The config key in the service manager
         */
        const FEATURE_KEY = null;
        /**
         * @var \Doctrine\ORM\EntityManager
         */
        private $entityManager = null;
        /**
         * @var null|array
         */
        private $config = null;

        /**
         * loads config and checks if feature is enabled
         *
         * @return void
         */
        public function init()
        {

            if (!$this->getConfig()->offsetGet('enabled')) {
                return false;
            }

            return $this->initFeature();;
        }

        /**
         * Get the configuration for this feature
         *
         * @return ArrayObject
         */
        public function getConfig()
        {
            if (!$this->config) {
                $features     = $this->getServiceLocator()->get('Config')[self::CONFIG_KEY];
                $this->config = new \ArrayObject($features[$this::FEATURE_KEY]);
            }

            return $this->config;
        }

        /**
         * Initiates the feature
         *
         * @return $this
         */
        abstract protected function initFeature();

    }
