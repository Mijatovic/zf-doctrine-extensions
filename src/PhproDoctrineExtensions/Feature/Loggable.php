<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Feature;

    class Loggable extends AbstractFeature
    {
        /**
         * The config key in the service manager
         */
        const FEATURE_KEY = 'loggable';

        /**
         * Initiates the feature
         *
         * @return $this
         */
        protected function initFeature()
        {
            $listener = new \Gedmo\Loggable\LoggableListener();

            if ($this->getConfig()->offsetExists('authenticationService')) {
                $authenticationServiceKey = $this->getConfig()->offsetGet('authenticationService');
                if ($this->getServiceLocator()->has($authenticationServiceKey)) {
                    /** @var \Zend\Authentication\AuthenticationService $authService */
                    $authService = $this->getServiceLocator()->get($authenticationServiceKey);
                    if ($authService->hasIdentity()) {
                        $authService->getIdentity();
                        //$listener->setUsername($authService->getIdentity());
                    }
                }
            }
            $this->getObjectManager()->getEventManager()->addEventSubscriber($listener);
            return true;
        }
    }
