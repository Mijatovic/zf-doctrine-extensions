<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Feature;

    use PhproDoctrineExtensions\Exception;;

    class Blameable extends AbstractFeature
    {

        /**
         * The config key in the service manager
         */
        const FEATURE_KEY = 'blameable';

        /**
         * Initiates the feature
         *
         * @return $this
         */
        protected function initFeature()
        {
            if (!$this->getConfig()->offsetExists('authenticationService')) {
                throw new Exception\ConfigurationException('blameable feature should have a authenticationService
                configured');
            }
            $authenticationServiceKey = $this->getConfig()->offsetGet('authenticationService');
            if ($this->getServiceLocator()->has($authenticationServiceKey)) {
                /** @var \Zend\Authentication\AuthenticationService $authService */
                $authService = $this->getServiceLocator()->get($authenticationServiceKey);
                if ($authService->hasIdentity()) {
                    $listener = new \Gedmo\Blameable\BlameableListener();
                    $listener->setUserValue($authService->getIdentity());
                    $this->getObjectManager()->getEventManager()->addEventSubscriber($listener);
                }
            }

            return true;
        }

    }
