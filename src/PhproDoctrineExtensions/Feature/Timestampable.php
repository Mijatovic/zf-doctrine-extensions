<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Feature;

    class Timestampable extends AbstractFeature
    {
        /**
         * The config key in the service manager
         */
        const FEATURE_KEY = 'timestampable';

        /**
         * Init feature
         *
         * @return void
         */
        protected function initFeature()
        {
            $listener = new \Gedmo\Timestampable\TimestampableListener();
            $this->getObjectManager()->getEventManager()->addEventSubscriber($listener);
            return true;
        }

    }
