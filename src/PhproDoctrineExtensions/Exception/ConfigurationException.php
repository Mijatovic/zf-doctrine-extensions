<?php
    /**
     * PhproDoctrineExtensions
     *
     * @link      https://bitbucket.org/phpro/phpro-doctrineextensions
     * @copyright Copyright (c) 2013 PHPro
     * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
     *
     */
    namespace PhproDoctrineExtensions\Exception;

    /**
     * Class ConfigurationException
     *
     * @package PhproSmartCrud\Exception
     */
    class ConfigurationException extends \Exception
    {

    }
